package org.geekhub.dt;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.Arrays;
import java.util.Comparator;

@javax.servlet.annotation.WebServlet(name = "StartServlet", urlPatterns = "/*")
public class StartServlet extends javax.servlet.http.HttpServlet {

    private static final String KEY_PATH = "key_path";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String filename = request.getParameter("filename");
        if (filename == null) return;

        HttpSession session = request.getSession();
        String path = (String) session.getAttribute(KEY_PATH);
        File file = new File(getFilePath(path, filename));
        if (file.exists()) {
            String text = request.getParameter("text");
            saveFile(file, text.getBytes("UTF-8"));
        } else {
            boolean result = file.createNewFile();
            if (result) {
                // TODO reload page
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter writer = response.getWriter();
        writer.println("<html>");
        printCss(writer);
        writer.println("<body>");
        printJs(writer);
        writer.println("<h2>File Manager</h2>");

        String path = request.getPathInfo();
        if (path.equals("/")) {
            path = "";
        }

        HttpSession session = request.getSession();
        File file = new File(getFolderPath(path));
        if (file.isDirectory()) {
            printCreateButton(writer, "Create file");
            session.setAttribute(KEY_PATH, path);
            printUpLink(writer, path);
            String[] dirList = file.list();

            Arrays.sort(dirList, new FileNameComparator());
            Arrays.sort(dirList, new FileTypeComparator(path));

            for (String s : dirList) {
                printLink(writer, path, s);
            }
        } else if (file.isFile() && isTextFile(file.getName())) {
//            session.setAttribute(KEY_PATH, path);
            openFile(file, writer);
        }

        writer.println("</body></html>");
    }

    private void openFile(File file, PrintWriter pw) throws IOException {
        InputStream is = new BufferedInputStream(new FileInputStream(file));
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader reader = new BufferedReader(isr);
        pw.println("<div><textarea id=\"fileText\" rows=\"20\" cols=\"80\">");
        String text;

        while ((text = reader.readLine()) != null) {
            pw.println(text);
        }
        pw.println("</textarea></div>");
        pw.println("<div><input type=\"submit\" value=\"Save file\" onclick=\"saveFile('" + file.getName() + "')\"/></div>");
    }

    private void saveFile(File file, byte[] bytes) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytes);
        fos.close();
    }

    private boolean isTextFile(String filename) {
        String extension = filename.substring(filename.lastIndexOf(".") + 1, filename.length());
        return extension.equalsIgnoreCase("txt");
    }

    private String getFilePath(String path, String filename) {
        if (path.equals("/")) {
            path = "";
        }

        return getFolderPath(path) + "/" + filename;
    }

    public void printCss(PrintWriter writer) {
        String css = "body { font-size: 14pt;} div { padding-top: 8px; }";
        writer.println("<style>");
        writer.println(css);
        writer.println("</style>");
    }

    private void printJs(PrintWriter writer) {
        String js = "<script>\n" +
                "function createFile() {\n" +
                "    var filename=prompt(\"Please enter file name\",\"file.txt\");\n" +
                "    if (filename!=null) {\n" +
                "        var request=new XMLHttpRequest();\n" +
                "        request.open(\"POST\", \"http://localhost:8080?filename=\"+filename, true);\n" +
                "        request.send(filename);\n" +
                "    }\n" +
                "}\n" +
                "function deleteFile() {\n" +
                "}\n" +
                "function saveFile(filename) {\n" +
                "     var text = document.getElementById('fileText').value;\n" +
                "     var request=new XMLHttpRequest();\n" +
                "     request.open(\"POST\", \"http://localhost:8080?filename=\" + filename + \"&text=\" + text, true);\n" +
                "     request.send(filename);\n" +
                "}\n" +
                "</script>";
        writer.println(js);
    }

    private void printUpLink(PrintWriter writer, String path) {
        if (path.equals("")) return;

        String[] strings = path.split("/");
        StringBuilder sb = new StringBuilder("");
        for (int i = 1; i < strings.length - 1; i++) {
            sb.append("/").append(strings[i]);
        }

        if (sb.length() == 0) {
            sb.append("/");
        }
        String linkTag = "<div><a href=\"" + sb + "\">..</a></div>";
        writer.println(linkTag);
    }

    private void printLink(PrintWriter writer, String path, String link) {
        StringBuilder sb = new StringBuilder("<div><a href=\"" + path + "/" + link + "\">");
        File file = new File(getFilePath(path, link));
        sb.append(link);
        sb.append("</a>");
        if (file.isFile()) {
            sb.append("<input type=\"submit\" value=\"Delete file\" onclick=\"deleteFile()\"/>");
        }
        sb.append("</div>");
        writer.println(sb);
    }

    private String getFolderPath(String path) {
        return Constants.BASE_PATH + path;
    }

    private void printCreateButton(PrintWriter writer, String text) {
        writer.println("<span><input type=\"submit\" value=\"" + text + "\" onclick=\"createFile()\"/></span>");
    }

    public String[] getDirList(HttpServletRequest request) {
        String path = request.getPathInfo();
        if (path == null || path.equals("/")) {
            path = "";
        }

        String[] dirList = new String[0];
        File file = new File(getFolderPath(path));
        if (file.isDirectory()) {
            dirList = file.list();
        } else {

        }

        return dirList;
    }

    class FileTypeComparator implements Comparator<String> {

        private String mPath;

        FileTypeComparator(String path) {
            mPath = path;
        }

        @Override
        public int compare(String o1, String o2) {
            File file1 = new File(getFilePath(mPath, o1));
            File file2 = new File(getFilePath(mPath, o2));

            if (file1.isDirectory() && file2.isFile())
                return -1;
            if (file1.isDirectory() && file2.isDirectory()) {
                return 0;
            }
            if (file1.isFile() && file2.isFile()) {
                return 0;
            }
            return 1;
        }
    }

    class FileNameComparator implements Comparator<String> {

        @Override
        public int compare(String file1, String file2) {
            return String.CASE_INSENSITIVE_ORDER.compare(file1, file2);
        }
    }
}
