<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="servlet" class="org.geekhub.dt.StartServlet" scope="session" />
<% String[] dirList = servlet.getDirList(request); %>
<html>
  <head>
    <title></title>
  </head>
  <body>
  <h2>File Manager</h2>
  <% for (String link : dirList) { %>
  <div><a href="index.jsp?<%= request.getPathInfo() %>+<%= link %>"><%=link%></a></div>
  <%
      }
  %>
  </body>
</html>