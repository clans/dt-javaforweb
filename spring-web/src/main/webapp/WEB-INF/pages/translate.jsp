<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <form:form method="POST" action="/result" modelAttribute="data">
        <form:textarea path="text" rows="15" cols="60" /><br />
        <input type="submit" />
    </form:form>
</body>
</html>