package org.geekhub.mvc;

import org.geekhub.mvc.lang.Language;
import org.geekhub.mvc.lang.LanguageDetector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component("translator")
public class Translator {

    @Autowired
	private Dictionary dictionary;
    @Autowired
	private LanguageDetector languageDetector;

    @PostConstruct
    public void init() {

    }

    public String translate(String source) {
		Language language = languageDetector.detectLanguage(source);
		String[] words = source.split(" ");
		StringBuilder sb = new StringBuilder();
		for (String word : words) {
			String translatedWord = dictionary.translate(word, language);
			sb.append(translatedWord + " ");
		}
		return sb.toString();
	}

}
