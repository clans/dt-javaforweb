package org.geekhub.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TranslateController {

    @Autowired
    private Translator translator;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String initTranslate(Model model) {
        model.addAttribute("data", new Data());
        return "translate";
    }

    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String translate(@ModelAttribute Data data, Model model) {
        String translate = translator.translate(data.getText());
        model.addAttribute("result", translate);
        return "result";
    }
}