package org.geekhub.mvc;

import org.geekhub.mvc.lang.Language;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class Dictionary {

    @Autowired
    private ResourceLoader resourceLoader;

    @Autowired
    private HttpServletRequest request;

	private Map<Language, Map<String, String>> dictionaries = new HashMap<Language, Map<String, String>>();

	public String translate(String word, Language language) {
		Map<String, String> dictionary = getDictionary(language);
		String translation = dictionary.get(word);
		return translation == null ? word : translation;
	}

	private Map<String, String> getDictionary(Language language) {
		Map<String, String> dictionary = dictionaries.get(language);
		if (null == dictionary) {
			dictionary = loadDictionary(language);
			dictionaries.put(language, dictionary);
		}
		return dictionary;
	}

	private Map<String, String> loadDictionary(Language language) {
		Map<String, String> dict = new HashMap<String, String>();
        String path = request.getSession().getServletContext()
                .getRealPath("/resources/" + language.name().toLowerCase() + ".dict");
        List<String> lines = resourceLoader.load(path);
		for (String line : lines) {
			String[] parts = line.split("=");
			dict.put(parts[0], parts[1]);
		}
		return dict;
	}
}
