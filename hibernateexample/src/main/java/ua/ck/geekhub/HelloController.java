package ua.ck.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;

import java.util.List;

@Controller
public class HelloController {

	@Autowired
	UserService userService;
    @Autowired
    GroupService groupService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
//		model.addAttribute("message", "Hello world!");
		return "redirect:users";
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String user(ModelMap model) {
		model.addAttribute("users", userService.getUsers());
		return "hello";
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String createUser(@RequestParam String firstName, @RequestParam String lastName, @RequestParam String email) {
		userService.createUser(firstName, lastName, email);
		return "redirect:users";
	}

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveUser(@RequestParam String userId, @RequestParam String firstName,
                           @RequestParam String lastName, @RequestParam String email, @RequestParam String groupId) {
        Group group = groupService.getGroup(Integer.parseInt(groupId));
        User user = new User(Integer.parseInt(userId), firstName, lastName, email, group);
        userService.saveUser(user);
        return "redirect:users";
    }

    @RequestMapping(value = "/groups", method = RequestMethod.POST)
    public String createGroup(@RequestParam String groupName) {
        groupService.createGroup(groupName);
        return "redirect:users";
    }

    @RequestMapping("/edit-user")
    public String editUser(@RequestParam String id, ModelMap model) {
        User user = userService.getUser(Integer.parseInt(id));
        List<Group> groups = groupService.getGroups();
        model.addAttribute("user", user);
        model.addAttribute("groups", groups);
        return "edit-user";
    }

    @RequestMapping("/delete-user")
    public String deleteUser(@RequestParam String id) {
        User user = userService.getUser(Integer.parseInt(id));
        userService.deleteUser(user);
        return "redirect:users";
    }
}