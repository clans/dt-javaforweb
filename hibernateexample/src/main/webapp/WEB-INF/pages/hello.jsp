<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <link rel="stylesheet" href="<c:url value="/res/style.css" />">
    <script src="<c:url value="/res/jquery-2.1.0.min.js" />"></script>
    <script src="<c:url value="/res/jquery.validate.min.js" />"></script>
    <script src="<c:url value="/res/scripts.js" />"></script>
</head>
<body>
<div id="wrapper" style="width: 800px">
    <h1>Hibernate Example</h1>

    <div class="tabs-wrapper">
        <ul class="tabs">
            <li class="tab-link active" data-tab="tab-user">Add user</li>
            <li class="tab-link" data-tab="tab-group">Add group</li>
        </ul>
        <div id="tab-user" class="tab-content active">
            <form id="addUser" action="${pageContext.request.contextPath}/users" method="post">
                <table>
                    <tr>
                        <td><label class="error" for="fName"></label></td>
                        <td><label class="error" for="lName"></label></td>
                        <td><label class="error" for="email"></label></td>
                    </tr>
                    <tr>
                        <td><input id="fName" class="input" name="firstName" placeholder="First Name" minlength="2"
                                   required></td>
                        <td><input id="lName" class="input" name="lastName" placeholder="Last Name" minlength="2"
                                   required></td>
                        <td><input id="email" class="input" name="email" type="email" placeholder="Email" required></td>

                        <td><input class="button" type="submit" value="Submit"></td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="tab-group" class="tab-content">
            <form id="addGroup" action="${pageContext.request.contextPath}/groups" method="post">
                <table>
                    <tr>
                        <td><label class="error" for="name"></label></td>
                    </tr>
                    <tr>
                        <td><input id="name" class="input" name="groupName" placeholder="Group Name"
                                   minlength="2" required></td>

                        <td><input class="button" type="submit" value="Submit"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>

    <table id="usersTable">
        <tr class="table_header">
            <td style="width: 140px">ID</td>
            <td style="width: 140px">FIRST NAME</td>
            <td style="width: 140px">LAST NAME</td>
            <td style="width: 140px">EMAIL</td>
            <td style="width: 140px">GROUP</td>
            <td style="width: 100px">ACTIONS</td>
        </tr>

        <c:forEach var="user" items="${users}">
            <tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.email}</td>
                <td>${user.group.name}</td>
                <td>
                    <a href="edit-user?id=${user.id}">Edit</a>&nbsp;&nbsp;&nbsp;
                    <a href="delete-user?id=${user.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>

    </table>
</div>
</body>
</html>