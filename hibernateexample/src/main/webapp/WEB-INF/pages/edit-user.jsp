<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Edit user</title>
    <link rel="stylesheet" href="<c:url value="/res/style.css" />">
    <script src="<c:url value="/res/jquery-2.1.0.min.js" />"></script>
    <script src="<c:url value="/res/jquery.validate.min.js" />"></script>
    <script src="<c:url value="/res/scripts.js" />"></script>
</head>
<body>
<div id="wrapper" style="width: 200px">
    <form id="editUser" action="${pageContext.request.contextPath}/save" method="post">
        <input type="hidden" name="userId" value="${user.id}">
        <label class="error" for="fName"></label><br/>
        <input id="fName" class="input" name="firstName" value="${user.firstName}" minlength="2" required>
        <label class="error" for="lName"></label><br/>
        <input id="lName" class="input" name="lastName" value="${user.lastName}" minlength="2" required>
        <label class="error" for="email"></label><br/>
        <input id="email" class="input" name="email" type="email" value="${user.email}" required><br/>
        <select style="width: 200px" name="groupId" required>
            <option value="none">--- Select group ---</option>
            <c:forEach var="group" items="${groups}">
                <option value="${group.id}" ${group.id == user.group.id ? 'selected' : ''}>
                        ${group.name}</option>
            </c:forEach>
        </select>

        <div style="float: right">
            <input class="button" type="submit" value="Save">&nbsp;
            <input class="button" type="button" value="Cancel" onclick="window.history.back()"/>
        </div>
    </form>
</div>
</body>
</html>
