$(document).ready(function() {
    validate();
    tabs();
});

function validate () {
    $("#addUser").validate();
    $("#addGroup").validate();
    $("#editUser").validate();
}

function tabs() {
    $("ul.tabs li").click(function () {
        var tab_id = $(this).attr("data-tab");

        $("ul.tabs li").removeClass("active");
        $(".tab-content").removeClass("active");

        $(this).addClass("active");
        $("#" + tab_id).addClass("active");
    })

}