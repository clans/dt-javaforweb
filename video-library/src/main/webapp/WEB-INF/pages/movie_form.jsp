<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Add New Movie</title>
    <link rel="stylesheet" href="<c:url value="/resources/style.css" />">
</head>
<body>
<div id="wrapper" style="width: 400px">
    <div class="content">
        <form:form method="POST" action="/save-movie.html" modelAttribute="movie">
            <form:errors path="*" cssClass="errorblock" element="div" />
            <form:hidden path="id" />
            <table>
                <tr>
                    <td style="width: 110px"><form:label path="name">Film Name</form:label></td>
                    <td><form:input class="input" path="name"/></td>
                </tr>
                <tr>
                    <td><form:label path="year">Year</form:label></td>
                    <td><form:input class="input" path="year"/></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="float: right">
                            <input type="submit" name="action" value="${save_or_update}"/>&nbsp;&nbsp;
                            <input type="button" value="Cancel" onclick="window.history.back()"/>
                        </div>
                    </td>
                </tr>
            </table>
        </form:form>
    </div>
</div>
</body>
</html>
