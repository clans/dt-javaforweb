<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Add New Actor</title>
    <link rel="stylesheet" href="<c:url value="/resources/style.css" />">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>
        $(function() {
            $( "#datepicker" ).datepicker({
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
</head>
<body>
<div id="wrapper" style="width: 400px">
    <div class="content">
        <form:form id="actor" method="POST" action="/save-actor.html" modelAttribute="actor">
            <form:errors path="*" cssClass="errorblock" element="div"/>
            <form:hidden path="id" />
            <table>
                <tr>
                    <td style="width: 110px"><form:label path="firstName">First Name</form:label></td>
                    <td><form:input class="input" path="firstName"/></td>
                </tr>
                <tr>
                    <td><form:label path="lastName">Last Name</form:label></td>
                    <td><form:input class="input" path="lastName"/></td>
                </tr>
                <tr>
                    <td><form:label path="birthDate">Bidth Date</form:label></td>
                    <td><form:input id="datepicker" class="input" path="birthDate"/></td>
                </tr>
                <tr>
                    <td><form:label path="movieList">Movies</form:label></td>
                    <td>
                        <form:select class="input" path="movieList" multiple="true" size="6" required="true">
                            <form:options items="${movies}" itemValue="id" itemLabel="name"/>
                        </form:select>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="float: right">
                            <input type="submit" name="action" value="${save_or_update}"/>&nbsp;&nbsp;
                            <input type="button" value="Cancel" onclick="window.history.back()"/>
                        </div>
                    </td>
                </tr>
            </table>
        </form:form>
    </div>
</div>
</body>
</html>
