<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <title>Video library</title>
    <link rel="stylesheet" href="<c:url value="/resources/style.css" />">
    <script src="<c:url value="/resources/scripts.js" />"></script>
</head>
<body>
<div id="wrapper" style="width: 800px">
    <div id="actors">
        <div class="content">
            <div class="switchers">
                Actors&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" id="movies_link">Movies</a>
            </div>
            <table>
                <tr class="table_header">
                    <td style="width: 200px">First Name</td>
                    <td style="width: 200px">Last Name</td>
                    <td style="width: 200px">Birth Date</td>
                    <td style="width: 120px"># of Movies</td>
                    <td style="width: 40px"></td>
                    <td style="width: 40px"></td>
                </tr>
                <c:forEach items="${actors}" var="actor">
                    <tr>
                        <td>${actor.firstName}</td>
                        <td>${actor.lastName}</td>
                        <td><fmt:formatDate value="${actor.birthDate}" pattern="dd/MM/yyyy"/></td>
                        <td>${fn:length(actor.movieList)}</td>
                        <td><a href="#" onclick="editActor(${actor.id})">Edit</a></td>
                        <td><a href="delete-actor?id=${actor.id}">X</a></td>
                    </tr>
                </c:forEach>
            </table>
            <form action="<c:url value="/create-actor.html"/>" method="get">
                <input style="margin-top: 20px" type="submit" value="Add New Actor" />
            </form>
        </div>
    </div>
    <div style="display: none" id="movies">
        <div class="content">
            <div class="switchers">
                <a href="javascript:void(0)" id="actors_link">Actors</a>&nbsp;&nbsp;&nbsp;Movies
            </div>
            <table>
                <tr class="table_header">
                    <td style="width: 300px">Film Name</td>
                    <td style="width: 200px">Year</td>
                    <td style="width: 200px"># of Actors</td>
                    <td style="width: 40px"></td>
                    <td style="width: 40px"></td>
                </tr>
                <c:forEach items="${movies}" var="movie">
                    <tr>
                        <td>${movie.name}</td>
                        <td>${movie.year}</td>
                        <td>${fn:length(movie.actorList)}</td>
                        <td><a href="edit-movie?id=${movie.id}">Edit</a></td>
                        <td><a href="delete-movie?id=${movie.id}">X</a></td>
                    </tr>
                </c:forEach>
            </table>
            <form action="<c:url value="/create-movie.html"/>" method="get">
                <input style="margin-top: 20px" type="submit" value="Add New Movie" />
            </form>
        </div>
    </div>
</div>
</body>
</html>
