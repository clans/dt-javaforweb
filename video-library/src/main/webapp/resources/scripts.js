function swap(one, two) {
    document.getElementById(one).style.display = 'block';
    document.getElementById(two).style.display = 'none';
}

window.onload = function () {
    document.getElementById('actors_link').onclick = function () {
        swap('actors', 'movies');
    }

    document.getElementById('movies_link').onclick = function () {
        swap('movies','actors');
    }
}

function editActor(id) {
    var form = document.createElement("form");
    form.setAttribute("method", "POST");
    form.setAttribute("action", "/edit-actor.html");

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "id");
    hiddenField.setAttribute("value", id);

    form.appendChild(hiddenField);

    document.body.appendChild(form);
    form.submit();
}
