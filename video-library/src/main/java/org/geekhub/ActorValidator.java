package org.geekhub;

import org.geekhub.objects.Actor;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ActorValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Actor.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "required.firstName",
                "First Name is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "required.lastName",
                "Last Name is required!");
    }
}
