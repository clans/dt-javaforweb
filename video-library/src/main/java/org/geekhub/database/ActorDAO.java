package org.geekhub.database;

import org.geekhub.objects.Actor;
import org.geekhub.objects.Movie;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActorDAO {

    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void insert(final Actor actor) {
        Map<String, Object> map = new HashMap<String, Object>() {
            {
                put("first_name", actor.getFirstName());
                put("last_name", actor.getLastName());
                put("birth_date", actor.getBirthDate());
            }
        };

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(dataSource);
        Number number = jdbcInsert.withTableName("actor").usingGeneratedKeyColumns("id").executeAndReturnKey(map);
        actor.setId(number.intValue());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(dataSource).withTableName("movies_actors");

        List<Movie> movieList = actor.getMovieList();
        map = new HashMap<String, Object>();
        for (Movie movie : movieList) {
            map.put("actor_id", actor.getId());
            map.put("movie_id", movie.getId());

            insert.execute(map);
        }
    }

    public List<Actor> queryActors() {
        String sql = "SELECT * FROM actor";
        String moviesSql = "SELECT * FROM movie INNER JOIN movies_actors ON movie.id = movies_actors.movie_id WHERE " +
                "movies_actors.actor_id = ?";

        List<Actor> actorList = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Actor>(Actor.class));

        for (final Actor actor : actorList) {
            List<Movie> movieList = jdbcTemplate.query(moviesSql, new PreparedStatementSetter() {
                        @Override
                        public void setValues(PreparedStatement preparedStatement) throws SQLException {
                            preparedStatement.setString(1, String.valueOf(actor.getId()));
                        }
                    }, new RowMapper<Movie>() {
                        @Override
                        public Movie mapRow(ResultSet resultSet, int i) throws SQLException {
                            return new Movie(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3));
                        }
                    }
            );
            actor.setMovieList(movieList);
        }

        return actorList;
    }

    public Actor getById(final int id) {
        String sql = "SELECT * FROM actor WHERE actor.id = ?";
        String moviesSql = "SELECT * FROM movie INNER JOIN movies_actors ON movie.id = movies_actors.movie_id WHERE " +
                "movies_actors.actor_id = ?";

        Actor actor = jdbcTemplate.queryForObject(sql, new Object[]{id}, new BeanPropertyRowMapper<Actor>(Actor.class));
        List<Movie> movieList = jdbcTemplate.query(moviesSql, new Object[]{actor.getId()}, new RowMapper<Movie>() {
            @Override
            public Movie mapRow(ResultSet resultSet, int i) throws SQLException {
                return new Movie(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3));
            }
        });
        actor.setMovieList(movieList);
        return actor;
    }

    public void update(final Actor actor) {
        String sql = "UPDATE actor SET first_name = ?, last_name = ?, birth_date = ? WHERE id = ?";
        String deleteMoviesSql = "DELETE FROM movies_actors WHERE actor_id = ?";
        String createMoviesSql = "INSERT INTO movies_actors (movie_id, actor_id) VALUES (?, ?)";

        jdbcTemplate.update(sql, actor.getFirstName(), actor.getLastName(), actor.getBirthDate(), actor.getId());
        jdbcTemplate.update(deleteMoviesSql, actor.getId());
        jdbcTemplate.batchUpdate(createMoviesSql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Movie movie = actor.getMovieList().get(i);
                ps.setInt(1, movie.getId());
                ps.setInt(2, actor.getId());
            }

            @Override
            public int getBatchSize() {
                return actor.getMovieList().size();
            }
        });
    }

    public void delete(int id) {
        String sql = "DELETE FROM actor WHERE id = ?";
        String deleteMoviesSql = "DELETE FROM movies_actors WHERE actor_id = ?";

        jdbcTemplate.update(deleteMoviesSql, id);
        jdbcTemplate.update(sql, id);
    }
}
