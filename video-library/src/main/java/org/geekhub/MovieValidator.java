package org.geekhub;

import org.geekhub.objects.Movie;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class MovieValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return Movie.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required.name",
                "Film Name is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "year", "required.year",
                "Year is required!");

        Movie movie = (Movie) o;
        try {
            int year = Integer.parseInt(movie.getYear());
            if (year <= 0) {
                errors.rejectValue("year", "movie.year", "Year must be a positive number!");
            }
        } catch (NumberFormatException e) {
            errors.rejectValue("year", "movie.year", "Year must be a positive number!");
        }
    }
}
