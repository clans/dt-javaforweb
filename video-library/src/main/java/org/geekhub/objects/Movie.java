package org.geekhub.objects;

import com.sun.istack.internal.NotNull;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Component
public class Movie {

    private int id;
    @NotNull
    private String name;
    @NotNull
    private String year;
    private List<Actor> actorList;

    public Movie() {
    }

    public Movie(int id, String name, String year, List<Actor> actorList) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.actorList = actorList;
    }

    public Movie(int id, String name, String year) {
        this(id, name, year, new ArrayList<Actor>());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<Actor> getActorList() {
        return actorList;
    }

    public void setActorList(List<Actor> actorList) {
        this.actorList = actorList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (id != movie.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
