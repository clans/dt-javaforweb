package org.geekhub.objects;

import com.sun.istack.internal.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class Actor {

    private int id;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date birthDate;
    private List<Movie> movieList;

    public Actor() {
    }

    public Actor(int id, String firstName, String lastName, Date birthDate, List<Movie> movieList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.movieList = movieList;
    }

    public Actor(int id, String firstName, String lastName, Date birthDate) {
        this(id, firstName, lastName, birthDate, new ArrayList<Movie>());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }
}
