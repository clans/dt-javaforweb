package org.geekhub;

import org.geekhub.database.ActorDAO;
import org.geekhub.database.MovieDAO;
import org.geekhub.objects.Actor;
import org.geekhub.objects.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.beans.PropertyEditorSupport;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private MovieDAO movieDAO;
    @Autowired
    private ActorDAO actorDAO;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Movie.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                Movie movie = new Movie();
                movie.setId(Integer.parseInt(text));
                setValue(movie);
            }
        });
    }

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String showList(ModelMap model) {
        List<Movie> movies = movieDAO.queryMovies();
        List<Actor> actors = actorDAO.queryActors();
        model.addAttribute("movies", movies);
        model.addAttribute("actors", actors);
        return "list";
	}

    @RequestMapping(value = "/create-actor.html", method = RequestMethod.GET)
    public String createActor(ModelMap model) {
        List<Movie> movies = movieDAO.queryMovies();
        Actor actor = new Actor();
        model.addAttribute("actor", actor);
        model.addAttribute("movies", movies);
        model.addAttribute("save_or_update", "Save");
        return "actor_form";
    }

    @RequestMapping(value = "/create-movie.html", method = RequestMethod.GET)
    public String editMovie(ModelMap model) {
        model.addAttribute("movie", new Movie());
        model.addAttribute("save_or_update", "Save");
        return "movie_form";
    }

    @RequestMapping("/edit-movie")
    public String editMovie(@RequestParam int id, ModelMap model) {
        Movie movie = movieDAO.getById(id);
        model.addAttribute("movie", movie);
        model.addAttribute("save_or_update", "Update");
        return "movie_form";
    }

    @RequestMapping(value = "/save-movie.html", method = RequestMethod.POST)
    public String saveMovie(@RequestParam String action, @Valid @ModelAttribute("movie") Movie movie, BindingResult result,
                            ModelMap model) {
        MovieValidator movieValidator = new MovieValidator();
        movieValidator.validate(movie, result);

        if (result.hasErrors()) {
            model.addAttribute("movie", movie);
            model.addAttribute("save_or_update", "Save");
            return "movie_form";
        }

        if (action.equals("Save")) {
            movieDAO.insert(movie);
        } else {
            movieDAO.update(movie);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/save-actor.html", method = RequestMethod.POST)
    public String saveActor(@RequestParam String action, @Valid @ModelAttribute("actor") Actor actor, BindingResult result,
                            ModelMap model) {
        ActorValidator actorValidator = new ActorValidator();
        actorValidator.validate(actor, result);

        if (result.hasErrors()) {
            model.addAttribute("actor", actor);
            model.addAttribute("movies", movieDAO.queryMovies());
            model.addAttribute("save_or_update", "Save");
            return "actor_form";
        }

        if (action.equals("Save")) {
            actorDAO.insert(actor);
        } else if (action.equals("Update")) {
            actorDAO.update(actor);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/edit-actor.html", method = RequestMethod.POST)
    public String editActor(@RequestParam("id") int actorId, ModelMap model) {
        Actor actor = actorDAO.getById(actorId);
        model.addAttribute("actor", actor);
        model.addAttribute("movies", movieDAO.queryMovies());
        model.addAttribute("save_or_update", "Update");
        return "actor_form";
    }

    @RequestMapping("/delete-actor")
    public String deleteActor(@RequestParam int id) {
        actorDAO.delete(id);
        return "redirect:/";
    }

    @RequestMapping("/delete-movie")
    public String deleteMovie(@RequestParam int id) {
        movieDAO.delete(id);
        return "redirect:/";
    }
}