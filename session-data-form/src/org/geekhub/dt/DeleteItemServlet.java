package org.geekhub.dt;

import org.geekhub.dt.objects.Animal;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;

@WebServlet(urlPatterns = "/delete")
public class DeleteItemServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        TreeMap<String, List<Animal>> map = (TreeMap<String, List<Animal>>) session.getAttribute("map");

        int id = Integer.parseInt(request.getParameter("id"));
        Animal animalToDelete = null;
        for (List<Animal> animals : map.values()) {
            for (Animal animal : animals) {
                if (animal.getId() == id) {
                    animalToDelete = animal;
                }
            }

            if (animalToDelete != null) {
                animals.remove(animalToDelete);
                animalToDelete = null;
            }
        }
        session.setAttribute("map", map);
        request.getRequestDispatcher("/index.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
