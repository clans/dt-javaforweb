package org.geekhub.dt.taglib;

import org.geekhub.dt.objects.Animal;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;

public class SortedTableRowsList extends TagSupport {

    private TreeMap<String, List<Animal>> map;

    @Override
    public int doStartTag() throws JspException {
        JspWriter out = pageContext.getOut();
        for (String key : map.keySet()) {
            List<Animal> animals = map.get(key);
            for (int i = 0; i < animals.size(); i++) {
                boolean printName = (i == 0);
                try {
                    out.println(getFormattedRow(animals.get(i), printName));
                } catch (IOException e) {
                    throw new JspException("Error: " + e.getMessage());
                }
            }
        }

        return SKIP_BODY;
    }

    private String getFormattedRow(Animal animal, boolean printName) {
        String row = "<tr>" +
                "<td>" + (printName ? animal.getName() : "") + "</td>" +
                "<td>" + animal.getValue() + "</td>" +
                "<td><a href=\"#\" onclick=\"deleteItem(" + animal.getId() + ");\">Delete</a></td>" +
                "</tr>";
        return row;
    }

    public TreeMap<String, List<Animal>> getMap() {
        return map;
    }

    public void setMap(TreeMap<String, List<Animal>> map) {
        this.map = map;
    }
}
