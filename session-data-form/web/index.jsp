<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/SortedTableRowsListDescriptor.tld" prefix="row" %>
<html>
<head>
    <title>GeekHub - Form</title>
    <link type="text/css" rel="stylesheet" href="<c:url value="styles.css" />"/>
</head>
<script type="text/javascript">
    function deleteItem(id) {
        var form = document.createElement("form");
        form.setAttribute("method", "POST");
        form.setAttribute("action", "/delete");

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "id");
        hiddenField.setAttribute("value", id);

        form.appendChild(hiddenField);

        document.body.appendChild(form);
        form.submit();
    }

    function setFocus() {
        document.getElementById("inputName").focus();
    }
</script>
<body onload="setFocus()">
<div id="mainContent">
    <h2 style="text-align: center">Animals Form</h2>

    <form action="${pageContext.request.contextPath}/submit" method="POST">
        <table>
            <tr>
                <td class="tableHeader">Name</td>
                <td class="tableHeader">Value</td>
                <td class="tableHeader">Action</td>
            </tr>
            <c:if test="${not empty sessionScope.map}">
                <row:printRow map="${sessionScope.map}"/>
            </c:if>
            <tr>
                <td><input id="inputName" style="width: 210px" type="text" name="name"/></td>
                <td><input style="width: 210px" type="text" name="value"/></td>
                <td><input style="width: 50px" type="submit" value="Add"/></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>